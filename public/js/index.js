var itemList = document.getElementById('item-list');
var title = document.getElementById('title');

function getCollections(callback) {
  getJSON('/api/collections', function(err, data){
    callback(data);
  });
}

function getCollection(collection, callback) {
  getJSON('/api/collection/' + collection, function(err, data){
    callback(data);
  });
}

function clearItems() {
  itemList.innerHTML = "";
}

function createElement(element) {
  return document.createElement(element);
}

function showCollections() {
  function renderCollections(collections){
    for (var collection in collections) {
      addItem(collection, collections[collection].stack.length);
    }
  }

  function addItem(item, size){
    var li = createElement('li');
    var a = createElement('a');
    var span = createElement('span');
    span.innerHTML = ' <small>(' + size + ')</small>';
    a.innerHTML = item;
    a.onclick = function(){
      showCollection(item);
    };
    a.href = '#';
    li.appendChild(a);
    li.appendChild(span);
    itemList.appendChild(li);
  }

  clearItems();
  title.innerHTML = "INDIZEA";
  getCollections(renderCollections);
}

function showCollection(collection) {
  var stack;

  function renderCollection(collection){
    stack = collection.stack;
    for (var i = 0; i < collection.stack.length; i++) {
      addItem(collection.stack[i]);
    }
  }

  function addItem(item){
    var li = createElement('li');
    var span = createElement('span');
    var entzun = createElement('a');
    var ezabatu = createElement('a');
    span.innerHTML = item.substring(32 + collection.length);
    entzun.className = 'button is-small';
    entzun.innerHTML = 'entzun';
    entzun.href = item;
    entzun.setAttribute('target','blank');
    ezabatu.className = 'button is-warning is-small';
    ezabatu.innerHTML = 'ezabatu';
    ezabatu.onclick = function(){
        deleteItem(item);
    };
    li.appendChild(span);
    li.appendChild(entzun);
    li.appendChild(ezabatu);
    itemList.appendChild(li);
  }

  function deleteItem(item) {
    if(confirm("Ziur al zaude " + item + ' ezabatu nahi duzula?')) {
      stack.splice(stack.indexOf(item),1);
      postJSON('/api/' + collection, { stack : stack }, function(err, res){
        showCollection(collection);
      });
    }
  }

  clearItems();
  title.innerHTML = collection;
  getCollection(collection, renderCollection);
}


showCollections();
