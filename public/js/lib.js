function getJSON(url, callback){
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      var data = JSON.parse(this.response);
      return callback(null, data);
    } else {
      return callback(true, null);
    }
  };
  request.onerror = function() {
    return callback(true, null);
  };
  request.send();
}


function postJSON(url, data, callback) {
  var request = new XMLHttpRequest();
  request.open('POST', url, true);
  request.setRequestHeader('Content-Type', 'application/json');
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      return callback(null, true);
    } else {
      return callback(true, null);
    }
  };
  request.onerror = function() {
    return callback(true, null);
  };
  request.send(JSON.stringify(data));


}
