var Hose = require('flowr-hose');
var hapi = require('hapi');
var Store = require("jfs");
var Boom = require('boom');

var db = new Store(process.env.DATA_DIR || "/tmp/data");

var hose = new Hose({
  name: 'repeater',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("TTN.REPEATER is running...");

hose.process('push', process.env.CONCURRENCY || 1, function(job, done){
	push(job.data, function(err, response){
		console.log(err);
    console.log(response);
		done(null, response);
	});
});

hose.process('pop', process.env.CONCURRENCY || 1, function(job, done){
  pop(job.data, function(err, response){
    console.log(err);
    done(null, response);
  });
});

function push(data, cb){
  db.get(data.collection, function(err, obj){
    if (!obj) {
      obj = {stack:[]};
    }
    obj.stack.push(data.item);
    db.save(data.collection, obj, function(err){
      //ok
    });

	 return cb(null, true);

 });
}


function collection(data, cb){
  db.get(data.collection, function(err, obj){
    if (!obj) {
      obj = {stack:[]};
    }

   return cb(null, obj);
 });
}


function pop(data, cb){
  db.get(data.collection, function(err, obj){
    if (!obj) {
      obj = {stack:[]};
    }
    var item = obj.stack.shift();
    db.save(data.collection, obj, function(err){
      //ok
    });

   return cb(null, item);
 });
}

function update(data, cb){
    var obj = { stack: data.stack };
    db.save(data.collection, obj, function(err){
      //ok
    });

	 return cb(null, true);
}


// HTTP API
var server = new hapi.Server();
server.connection({ port: process.env.SERVER_PORT || 3000 });

server.handler('push', function (route, options) {
    return function (request, reply) {
      push({
        collection: request.params.collection,
        item: request.payload.url
        },
        function(err, success){
          return reply('OK');
        }
      );
    };
});


server.handler('pop', function (route, options) {
    return function (request, reply) {
        pop({
          collection: request.params.collection
        }, function(err, item){
          if(item){
            return reply.redirect(item);
          } else {
            return reply(Boom.notFound('missing'));
          }
        });
    };
});


server.handler('collection', function (route, options) {
    return function (request, reply) {
      collection({
        collection: request.params.collection
      }, function(err, item){
        return reply(item);
      });
    };
});

server.handler('collections', function (route, options) {
    return function (request, reply) {
      db.all(function(err, objs){
        return reply(objs);
      });
    };
});

server.handler('update', function (route, options) {
    return function (request, reply) {
      update({
        collection: request.params.collection,
        stack: request.payload.stack
      }, function(){
        // ok
      });

      return reply('ok');
    };
});

var routes = [
  {
    method: 'GET',
    path: '/api/{collection}/pop',
    config: {
    },
    handler: { pop: { msg: 'test' } }
  },
  {
    method: 'GET',
    path: '/api/collection/{collection}',
    config: {
    },
    handler: { collection: { msg: 'test' } }
  },
  {
    method: 'GET',
    path: '/api/collections',
    config: {
    },
    handler: { collections: { msg: 'test' } }
  },
  {
    method: 'POST',
    path: '/api/{collection}/push',
    config: {
    },
    handler: { push: { msg: 'test' } }
  },
  {
    method: 'POST',
    path: '/api/{collection}',
    config: {
    },
    handler: { update: { msg: 'test' } }
  }
];

server.route(routes);

server.register(require('inert'), function(err) {

    if (err) {
        throw err;
    }


    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: './public',
                redirectToSlash: true,
                index: true
            }
        }
    });
});

server.start(function () {
  console.log('Server running at:', server.info.uri);
});
